#include <iostream>
#include <cstdlib>

using namespace std;

extern "C" void detectHardware( int device ){

    int num_devices = 0;

    cudaGetDeviceCount(&num_devices);

    if( num_devices == 0){

         cout << ">> This computer does not have a CUDA capable device , ABORTING..." << endl;
         exit( EXIT_FAILURE );
    
    }

	if( device >= num_devices )
    {
         cout << ">> Invalid device id (" << device << ") . ABORTING... " << endl;
         exit(EXIT_FAILURE);
    }

    cudaSetDevice( device );

    cudaDeviceProp deviceProp;
    cudaGetDeviceProperties(&deviceProp, device);

    cout << ">> Using '" << deviceProp.name << "' as CUDA Device" << endl;

}