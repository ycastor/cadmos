/*=========================================================*
 *                        INF - UFG                        *
 *              Cadmos - Cuda Accelerated Data Mining      *
 *                   Licenced under Gnu/gplv3              *
 *=========================================================*/

#include <iostream>
#include <vector>
#include <cstdlib>
#include <cmath>

/* CADMOS */
#include "../common/input/input.h"
#include "helper_functions.h"
#include "helper_cuda.h"

using namespace std;

/* Declaration forward */
__inline__ __device__ int cuNotAlpha( char c );

__global__ void processBuffer( char* buffer , int buffer_size ){

    int idx = blockIdx.x * blockDim.x + threadIdx.x;
 
    if( idx > buffer_size)
       return;
  
    if(cuNotAlpha(buffer[idx]))
         buffer[idx] = ' ';
         
    __syncthreads();
    
}

extern "C" void categorizer( DirectoryFiles *dir_files , int entries ){

     /* Host Vars */
     char* h_buffer;
     int* h_sectors;
     long buffer_size;
     int numsectors;
     
     /* Device Vars */
     int* d_sectors;
     char* d_buffer;
     
     /* TODO: Do this async using streams */
     for(int i = 0 ; i < entries; i++) {
     
        buffer_size = inputRead( dir_files[i].src , h_buffer );
        h_sectors = sectorizeBuffer( h_buffer , buffer_size , 1 , numsectors );
    
        checkCudaErrors( cudaMalloc( &d_buffer  , buffer_size*sizeof(char) ) );
        checkCudaErrors( cudaMalloc( &d_sectors , numsectors*sizeof(int) ) );
     
        checkCudaErrors( cudaMemcpy( d_buffer , h_buffer , buffer_size*sizeof(char) , cudaMemcpyHostToDevice ) );
        checkCudaErrors( cudaMemcpy( d_sectors , h_sectors , numsectors*sizeof(int) , cudaMemcpyHostToDevice ) );
        
        // Blocks for the first kernel
        dim3 NUM_BLOCKS((buffer_size/512)+1);
        dim3 NUM_THREADS(512);
                       
        // Remove symbols kernel
        processBuffer<<<NUM_BLOCKS,NUM_THREADS>>>( d_buffer , buffer_size );
     
        // Free Host           
        free(h_sectors);
        free(h_buffer);
         
        //TODO: Anotha Kernel
        
                
        // Free Device
        checkCudaErrors( cudaFree(d_buffer) );
        checkCudaErrors( cudaFree(d_sectors) );
    }
 }

__inline__ __device__ int cuNotAlpha( char c ){

    if((('!' <= (c)) && ((c) <= '/')) || ((':' <= (c)) && ((c) <= '@')) || 
       (('[' <= (c)) && ((c) <= '`')) || (('{' <= (c)) && ((c) <= '~')) )
        return 1;
    else
        return 0;

}
