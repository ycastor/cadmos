#include <fstream>
#include <iostream>
#include <vector>
#include <cstdlib>
#include <cstring>

#include "input.h"

using namespace std;

long plaintext2Buffer( char* fpath , char *&fwords ){
	
	FILE* fpointer;
	long fsize = 0;
	
	fpointer = fopen( fpath , "r" );
	
	fseek(fpointer, 0, SEEK_END);
	fsize = ftell(fpointer);
    rewind(fpointer);
      
    fwords = (char*) malloc( (fsize + 1)*sizeof(char) );
    
    fread( fwords , fsize, 1, fpointer );
    
    fclose(fpointer);
    
    return fsize;

		
}
