/*=========================================================*
 *                        INF - UFG                        *
 *              Cadmos - Cuda Accelerated Data Mining      *
 *                   Licenced under Gnu/gplv3              *
 *=========================================================*/

#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <cstring>
#include <vector>

#include "input.h"
#include "input_text.h"

#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>


static void fix_dtype(char *path, struct dirent *dp);
static char* filepath( char* path , char* fname );

static DIR *dir = NULL;
static int num_files = 0;

using namespace std;

// Read Functor
long (*inputFunc)( char* , char *& );

/*
 * Support to Future Modules
 */ 
void inputConfigure( string *format )
{
	if (!format->compare("text")) {
		inputFunc = plaintext2Buffer;
	} else {
		cerr << ">> inputConfigure: Unknown Input type '" << format->c_str() << "' using 'text' instead";
		inputConfigure( new string("text") );
	}
}

int inputOpen(string *p , DirectoryFiles *&dir_files )
{
    assert(p);
    
	struct dirent *dp;
	char* path = strdup( p->c_str() );
		
    dir = opendir(path);
    
	if (!dir) {
        
		cout << ">> inputOpen: Could not open directory " << path << endl;
        return -1;
    
	}
	    
	while (dir && (dp = readdir(dir)) != NULL) {
        
		fix_dtype(path, dp);
        
		// Check the file and set the pointers to them using a structure
		if (dp->d_type == DT_REG || dp->d_type == DT_LNK){
            
			dir_files = (DirectoryFiles*)realloc( dir_files , (num_files+1)*sizeof(DirectoryFiles) );
			
			if(dir_files == NULL){
				cout << ">> inputOpen: Malloc Failed! Finishing with code 1" << endl;
				exit(EXIT_FAILURE);
			}
			
			dir_files[num_files].name = strdup( dp->d_name );
			dir_files[num_files].src = strdup( filepath( path , dp->d_name ) ) ;
			
			num_files++;
    	}
	
	}
    
	rewinddir(dir);
	
	return num_files;
}

void inputClose( DirectoryFiles *dir_files )
{
	
	for( int i = 0; i < num_files; i++ ){
			free( dir_files[i].name );
			free( dir_files[i].src );
	}	
		
	free(dir_files);
	closedir(dir);
}

long inputRead( char* fpath , char *&fwords ){

	return inputFunc( fpath , fwords );

}

static char* filepath( char* path , char* fname ){
	
	assert(path);
	static char filepath[254];
	
	if (fname)
		sprintf(filepath,"%s/%s", path, fname);
	else
		sprintf(filepath,"%s", path );
		
	return filepath;
	
	
}

static void fix_dtype(char *path, struct dirent *dp)
{
    struct stat st;
    char buffer[512];

    if (dp->d_type == DT_UNKNOWN) {
        sprintf(buffer, "%s/%s" , path, dp->d_name);
        stat(buffer, &st);
        if (S_ISREG(st.st_mode))
            dp->d_type = DT_REG;
        if (S_ISLNK(st.st_mode))
            dp->d_type = DT_LNK;
    }
}

int* sectorizeBuffer( char* buffer , long buffersize , long shift , int &numblocks ){

   int *sector;

   numblocks = 0;
         
   sector = (int*) malloc( 1 * sizeof(int) ); 
   
   for( long i = 0  ; i <= buffersize;  i++ ){
   	      
       /* Case 1: The section start is not a word */
       if( !isalpha( buffer[i] ) )
       	   continue;

       /* Case 2: The section start is at the middle of a word */
       if( isalpha( buffer[i-1] ) )
       	   continue;

       /* Is is a alpha character and not in the middle-end of a word 
          then it's a word start, as needed */
       
        sector[numblocks] = i;
        i += shift;
        numblocks++;

        sector = (int*) realloc( sector , (numblocks+1)*sizeof(int) );         

   }

   return sector;

} 