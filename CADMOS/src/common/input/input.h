/* ==========================================================================*
 *                   CADMOS - Cuda Accelerated Data Mining                   *
 *						   Licensed under Gnu/GPL V3                         *
 *                    http://www.gnu.org/licenses/gpl.txt                    *
 * ==========================================================================*/

#ifndef INPUT_H
#define INPUT_H

#include "../../cuda/gpustring.h"

typedef struct {

	char* name;
	char* src;

}DirectoryFiles;


int inputOpen(std::string* , DirectoryFiles*& );
void inputClose( DirectoryFiles* );
void inputConfigure( std::string* );
long inputRead( char* , char *& );
int* sectorizeBuffer( char* buffer , long buffersize , long shift , int &numblocks );


#endif
