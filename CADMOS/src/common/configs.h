/* ==========================================================================*
 *                   CADMOS - Cuda Accelerated Data Mining                   *
 *						   Licensed under Gnu/GPL V3                         *
 *                    http://www.gnu.org/licenses/gpl.txt                    *
 * ==========================================================================*/
#ifndef CONFIG_H
#define CONFIG_H

#include <libconfig.h++>

using namespace libconfig;
using namespace std;


/*
 * Union with constructor, C++11 Spec
 */
typedef union Values{

	long num;
	int _int;
	char* str;
	double dbl;

	Values( long n ){ num = n; }
	Values( char* s ) { str = s; }
	Values( double d ) { dbl = d; }
	Values( int integer ){ _int = integer; }

}s_val;

/* Config Structure */
typedef struct
{
	char *group;        
	char *name;         
	int type;

	s_val* val;

} config_structure;

void setDefaultConf( Config *config );

#endif
