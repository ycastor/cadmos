/* ==========================================================================*
 *                   CADMOS - Cuda Accelerated Data Mining                   *
 *						   Licensed under Gnu/GPL V3                         *
 *                    http://www.gnu.org/licenses/gpl.txt                    *
 * ==========================================================================*/
#include <iostream>
#include <cassert>

#include "configs.h"

using namespace std;
  
/*
 * Default configs
 * group , config name , config type , config value
 */
static config_structure defaults[] = {
	 
	{ "input", "input_type" , Setting::TypeString , new s_val("text") },
	{ "cuda", "cuda_device" , Setting::TypeInt , new s_val( (int)0 ) },
	{NULL}

};


/*
 * Initialize the program with default configs
 * @param	Config	Config File
 */
void setDefaultConf( Config *config ){

	Setting *cs;
   
	for( int i = 0; defaults[i].name; i++ ){
		
		if( !config->exists(defaults[i].group) )
		   cs = &config->getRoot().add( defaults[i].group , Setting::TypeGroup );
		else
		   cs = &config->lookup(defaults[i].group );
		
	switch(defaults[i].type){

		case Setting::TypeString:
			
			if( cs->exists( defaults[i].name ) )
				continue;

			cs->add( defaults[i].name , Setting::TypeString ) = defaults[i].val->str ;
			break;

		case Setting::TypeInt64:
			
			if( cs->exists( defaults[i].name ) )
				continue;

			cs->add( defaults[i].name , Setting::TypeInt64 ) = defaults[i].val->num ;
			break;

		case Setting::TypeInt:
			
			if( cs->exists( defaults[i].name ) )
				continue;

			cs->add( defaults[i].name , Setting::TypeInt ) = defaults[i].val->_int ;
			break;

		case Setting::TypeFloat:

			if( cs->exists( defaults[i].name ) )
				continue;

			cs->add( defaults[i].name , Setting::TypeFloat ) = defaults[i].val->dbl ;
			break;

			

		}
	}
}


