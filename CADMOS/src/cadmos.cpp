/* ==========================================================================*
 *                    CADMOS - Cuda Accelerated Data Mining                  *
 *                         Licensed under Gnu/GPL V3                         *
 *                    http://www.gnu.org/licenses/gpl.txt                    *
 * ==========================================================================*/
#include <iostream>
#include <cstdlib>
#include <vector>

#include "common/configs.h"
#include "common/input/input.h"

#ifdef _WIN32
#include "common/getopt.h"
#else
#include <getopt.h>
#endif

using namespace std;
using namespace libconfig;

// CUDA Functions
extern "C" void categorizer( DirectoryFiles *dir_files , int entries );
extern "C" void detectHardware( int device );

// OPT String
#define OPTSTRING  "i:c:hA"
#define P_VERSION "PRE-ALPHA"

// I/O
static char *input = NULL;
static char *output = NULL;

// Opened Files
static DirectoryFiles* dir_files;
static int entries = 0;

// CFG File
Config cfg;


/*
 *  Command Line Parameters
 */
static struct option params[] = {
	
	{"input_type", 1, NULL, 'i'},
	{"cuda_device", 1 , NULL , 'c'},
	{"about", 0, NULL, 'A'},
	{"help", 0, NULL, 'h'},
	{ NULL , 0 , NULL , 0 }

};

void cadmosAbout()
{
	cout << " CADMOS " << P_VERSION << endl
		 << " TF-IDF Weighting using GPU Acceleration " << endl;
}

void cadmosHelp()
{
	cout << "CADMOS Usage: "
		 << "cadmos [options] <input> <output> " << endl
		 << endl << "  File Options: " << endl
		 << "    -i --input_type ( text , csv , matlab )" << endl
		 << endl << "  General Options:" << endl
		 << "    --about Shows About CADMOS" << endl
		 << "    --help Shows this menu" << endl;

}

/*
 *  Command Line Parameters Parser
 */
static void cadmosReadParams( int argc , char** argv ){

	int ch;
	optind = 0;

	while ((ch = getopt_long(argc, argv, OPTSTRING, params, NULL)) != -1) {

		switch(ch){

		case 'A':
			cadmosAbout();
			exit(EXIT_SUCCESS);

		case 'h':
			cadmosHelp();
			exit(EXIT_SUCCESS);

		case 'i':
			cfg.lookup("input.input_type") = optarg;
			break;

		case 'c':
		    cfg.lookup("cuda.cuda_device") = optarg;
		    break;
		
		}

	}

	argc -= optind;
	argv += optind;

	if (argc != 2) {
		cadmosHelp();
		exit(EXIT_FAILURE);
	} else {
		input = argv[0];
		output = argv[1];
	}

	return;

}

/*
 *  Parse Parameters and Initialize the tool
 */
void cadmosInit(){

	const char* type;
    int device;

    cfg.lookupValue( "cuda.cuda_device" , device );
    detectHardware( device );  
						
	cfg.lookupValue( "input.input_type" , type );
	cout << ">> Opening '" <<  input  << "' using '" <<  type  << "' as input module"  << endl;
	
	inputConfigure( new string(type) );

	entries = inputOpen( new string(input) , dir_files );

	if( entries <= 0){
		cerr << ">> No files detected" << endl;
		exit(EXIT_FAILURE);
	}
		
	cout << ">> Found '" << entries << "' files" << endl;

	
}

void cadmosExecute(){
	
	categorizer( dir_files , entries );
        
}



static void cadmosFinish(){

	inputClose(dir_files);
		
}

int main( int argc , char** argv ){

	setDefaultConf(&cfg);
	cadmosReadParams( argc , argv );
	cadmosInit();
	cadmosExecute();
	cadmosFinish();
	
	
	exit(EXIT_SUCCESS);

}
