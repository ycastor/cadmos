#=========================================================#
#                        INF - UFG                        #
#           Cadmos - Cuda Accelerated Data Mining         #
#                Maintained By: Ygor Castor               #
#=========================================================#                            

# Libraries Includes
INCLUDE_DIRECTORIES(
   ${CUDA_INCLUDE_DIRS}
   ${CUDA_CUT_INCLUDE_DIR}
   ${LIBCONFIGPP_INCLUDE_DIR}
)

set(CUDA_ATTACH_VS_BUILD_RULE_TO_CUDA_FILE OFF)
add_definitions(-DMULTIPLIER=2)

# All Non-Cuda files goes here
set(main_files 

	${CMAKE_CURRENT_SOURCE_DIR}/cadmos.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/common/configs.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/common/configs.h
	${CMAKE_CURRENT_SOURCE_DIR}/common/input/input.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/common/input/input.h
	${CMAKE_CURRENT_SOURCE_DIR}/common/input/input_text.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/common/input/input_text.h
		
)

# All Cuda files goes here
set(cuda_files
    ${CMAKE_CURRENT_SOURCE_DIR}/cuda/categorizer.cu
    ${CMAKE_CURRENT_SOURCE_DIR}/cuda/detecthardware.cu
    ${CMAKE_CURRENT_SOURCE_DIR}/cuda/pattern.h
)

# The CUDA_ADD_EXECUTABLE finds the suited compiler
CUDA_ADD_EXECUTABLE(cadmos
      ${cuda_files}
      ${main_files}
)

# Link the libraries to the executable
TARGET_LINK_LIBRARIES(cadmos
 
      ${CUDA_LIBRARIES}
      ${LIBCONFIGPP_LIBRARY}
 
)

# Clean nvcc useless archives
CUDA_BUILD_CLEAN_TARGET()




